# Dicas de como realizar um tuning no SQL Serve



Quando vamos iniciar um trabalho de tuning, uma das primeiras informações que precisamos é visualizar o plano de execução da query e as informações de estatísticas.

Mas antes vamos entender um pouco os passos que o **Otimizador de Consultas** segue para executar uma query.

![](img/OTP.jpg)

Primeiro o Otimizador de Consultas faz o “Parsing“ do comando para validar se a sintaxe do comando está correta.

O código estando correto, é gerado o que chamamos de parser tree, que contém os passos lógicos para a execução da consulta SQL.

O segundo passo é o que chamamos de Algebrizer. Essa é a etapa que faz a normalização do comando que recebe do parser tree, verifica a existência dos objetos utilizados na consulta (tabelas, colunas, etc), além de outras tarefas, como validar se o usuário que está executando tem permissão de acesso, valida as constraints e etc. A saída desse passo é um metadado com as informações.

O terceiro passo é o que chamamos de Optimizer, que vai fazer a leitura do metadado recebido da fase anterior e vai começar a analisar os operadores físicos e lógicos que ele vai utilizar. Também vai analisar os hints que estão na query e os operadores de JOIN.

Após isso, ele vai começar a gerar vários planos de execução. Dentre os planos gerados, ele vai escolher o que for classificado como bom o suficiente “good enough“.

Para ele escolher esse plano ele leva em consideração o custo de cada operação, além do custo de recursos como IO e CPU.

Uma vez que o otimizador de consultas gera o plano de execução, ele é então armazenado em uma área de memória denominada plan cache, que é uma área onde ficam armazenados todos os planos de execução do SQL Server.

Concluído o terceiro passo, agora que o nosso plano de execução está gerado, o SQL Server sabe exatamente como chegar nos dados, então ele é colocado na fila de execução que é o passo Execution ou Exec Plan.

## Plano de Execução

!!! Quote "Pergunta"
    **O que seria o plano de execução?**
Apesar de quase 100% das pessoas que trabalham com SQL Server saberem o que é o plano de execução e pra quê ele serve, se você está começando a trabalhar com banco de dados agora, vou dar uma resumida.

O primeiro ponto é que o Plano de Execução não é exclusivo do SQL Server – todos os SGBDs que eu conheço têm planos de execução (Oracle, MySQL, Postgree, etc), que nada mais é do que uma representação gráfica, textual ou em XML que mostra quais operadores e operações que o otimizador de consultas fez para retornar os dados da sua query e também o custo de cada operação realizada.

![](img/pmc1.jpg)

Com esse recurso você consegue ver as etapas de execução da sua query e assim verificar o que pode ser melhorado.

Para gerar um plano de execução de uma instrução, você pode simplesmente utilizar o atalho “**Ctrl + M**” ou então clicar no ícone destacado na imagem abaixo:

![DV](img/DV.jpg)

O plano de execução também pode nos dar informação de sugestão de índices do SQL Server.

!!! Quote ""
    **Mas o que seriam essas sugestões de criação de índices?**

Por exemplo, quando executamos uma consulta, o otimizador de consultas utiliza as estatísticas e identifica que a query que você está executando teria um desempenho melhor se a tabela X tivesse um índice na coluna Y.

![](img/yellw.jpg)

Como podemos observar na imagem acima, o otimizador de consultas está nos sugerindo um índice na tabela “**Sales.Customer**“, que vai gerar um impacto de “97%” na execução da consulta.

Para fazer a leitura do plano de execução, as literaturas recomendam que devemos começar da esquerda para a direita e de cima para baixo, uma vez que isso facilita a nossa interpretação.

Porém, o **Otimizador de Consultas constrói os planos de execução da direita para a esquerda e de cima para baixo**. Também precisamos aprender o que significa cada operador ou pelo menos os principais deles.

Para consultar os operadores podemos consultar o Books Online ([BOL](https://docs.microsoft.com/pt-br/sql/relational-databases/showplan-logical-and-physical-operators-reference?view=sql-server-2017)).

Entenderemos melhor com um plano de execução um pouco mais complexo. Vamos exibir o plano de execução da query abaixo:

```t-sql
WITH
LastDayOrderIDs AS
(
SELECT SalesOrderID FROM Sales.SalesOrderHeader
WHERE OrderDate = (SELECT MAX(OrderDate) FROM Sales.SalesOrderHeader)
),
LastDayProductQuantities AS
(
SELECT ProductID, SUM(OrderQty) AS LastDayQuantity
FROM Sales.SalesOrderDetail
JOIN LastDayOrderIDs
ON Sales.SalesOrderDetail.SalesOrderID = LastDayOrderIDs.SalesOrderID
GROUP BY ProductID
),

LastDayProductDetails AS
(
SELECT
Product.ProductID,
Product.Name,
LastDayQuantity
FROM Production.Product
JOIN LastDayProductQuantities
ON Product.ProductID = LastDayProductQuantities.ProductID
)

SELECT * FROM LastDayProductDetails
ORDER BY LastDayQuantity DESC;
```

## Tasklist

- [x] Lorem ipsum dolor sit amet, consectetur adipiscing elit
- [ ] Vestibulum convallis sit amet nisi a tincidunt
  - [x] In hac habitasse platea dictumst
  - [x] In scelerisque nibh non dolor mollis congue sed et metus
  - [ ] Praesent sed risus massa
- [ ] Aenean pretium efficitur erat, donec pharetra, ligula non scelerisque

## Tabs

=== "Unordered list"

    * Sed sagittis eleifend rutrum
    * Donec vitae suscipit est
    * Nulla tempor lobortis orci

=== "Ordered list"

    1. Sed sagittis eleifend rutrum
    2. Donec vitae suscipit est
    3. Nulla tempor lobortis orci
